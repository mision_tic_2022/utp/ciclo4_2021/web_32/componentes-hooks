import { Route, Routes } from "react-router";
import Dashboard from "../components/Dashboard";
import MiPrimerComponente from "../components/MiPrimerComponente";

const AuthRouter = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Dashboard/>}>
                    <Route index element={<MiPrimerComponente/>}/>
                </Route>
            </Routes>
        </>
    )
}

export default AuthRouter;
