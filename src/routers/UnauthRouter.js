import { Route, Routes } from "react-router";
import Login from "../components/Login";

const UnauthRouter = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Login/>}></Route>
            </Routes>
        </>
    )
}

export default UnauthRouter;
