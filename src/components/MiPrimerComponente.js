import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useEffect, useState } from 'react';
import { RecibirProps } from './RecibirProps';

export default function MiPrimerComponente() {

    const [num1, setNum1] = useState("");
    const [num2, setNum2] = useState("");
    const [result, setResult] = useState("");

    //Punto de montaje
    useEffect(()=>{
        console.log("Creación del componente");
        //alert('Hola Andrés!');
    }, []);

    useEffect(()=>{
        console.log("Actualizando el segundo input");
        //alert('Actualizando...');
    }, [num2]);


    const handleNum1 = (e)=> setNum1(e.target.value);
    
    const handleNum2 = (e)=> setNum2(e.target.value);

    //Julián Andrés Gamarra
    const handleSubmit = (e) => {
        e.preventDefault();    
        if (!(num1 === "") || !(num2 === "")) {
          setResult(parseInt(num1) + parseInt(num2));
        } else {
          console.log("Faltan campos");
        }
      };

    return (
        <>
            <h1>Mi Primer Componente con Hooks</h1>
            {/**Formulario calculadora*/}
            <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="formNum1">
                    <Form.Control value={num1} type="number" onChange={handleNum1} placeholder="Ingrese el primer número" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formNum2">
                    <Form.Control value={num2} onChange={handleNum2} type="number" placeholder="Ingrese el segundo número" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Sumar
                </Button>
            </Form>
            {!(result === "") ? (<RecibirProps result={result}/>): (<></>)}
            
            
        </>
    );

}